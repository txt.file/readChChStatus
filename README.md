# readChChStatus

Java Program aus dem Jahr 2014 für den Chaostreff Chemnitz, um den Türstatus (geöffnet/geschlossen) auszulesen.

Dieses Projekt steht unter der [The Unlicense](https://unlicense.org/).
