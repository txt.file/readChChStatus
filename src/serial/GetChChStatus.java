/**
 * SPDX-License-Identifier: Unlicense
 */
package serial;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Logger;

/**
 * @author txt
 * @version 0.0.1
 */
public class GetChChStatus {
        
        boolean getChChStatus() {
                Logger log = Logger.getAnonymousLogger();
                boolean ret = false;
                try {
                        for (String line : Files.readAllLines(
                                        Paths.get("/proc/tty/driver/serial"),
                                        Charset.defaultCharset())) {
                                if (line.contains("RI")) {
                                        ret = true;
                                        break;
                                }
                        }
                } catch (AccessDeniedException e) {
                        log.severe("Access to " + e.getFile() + " denied.");
                } catch (IOException e) {
                        log.severe(e.getMessage());
                }
                return ret;
        }
}
