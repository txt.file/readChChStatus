/**
 * SPDX-License-Identifier: Unlicense
 */
package serial;

import java.util.logging.Logger;

/**
 * @author txt
 * @version 0.0.1
 */
public class Main {
        
        /**
         * @param args
         */
        public static void main(String[] args) {
                Logger log = Logger.getAnonymousLogger();
                GetChChStatus g = new GetChChStatus();
                boolean status = g.getChChStatus();
                if (status) {
                        log.info("geöffnet");
                        System.out.println("geöffnet");
                } else {
                        log.info("geschlossen");
                        System.out.println("geschlossen");
                }
        }
        
}
